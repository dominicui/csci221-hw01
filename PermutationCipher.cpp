//============================================================================
// Name        : j.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include <algorithm>
using namespace std;

int length;
int blockSize;
int fillX;
int deleteX = 0;
int permutation[4];
int depermutation[4];
int possiblePermutation[4];
string plaintext;
string block;
string in;
string encode;

void check(int[]);
void sort(int[]);
void encrypt(int[]);
void decrypt(int[]);
void possibleP();

int main()
{
	cout << "Type in your plaintext:" << endl;
	getline(cin, plaintext);
	length = plaintext.length();
	//get input message

	cout << "Enter your block size n (1-4):" << endl;
	getline(cin, block);
	blockSize = atoi(block.c_str());
	//get block size

	cout << "Enter your permutation from 1 to n:" << endl;
	getline(cin, in);
	//get permutation



	check(permutation);
	sort(permutation);
	encrypt(permutation);
    decrypt(depermutation);
    possibleP();
}

void check(int c[])
{
	for(int i = 0; i < blockSize; i++)
	{
		permutation[i] += in[i] - 48;

		if(permutation[i] > blockSize)
		{
			cout << "Every permutation number must less than block size n, re-run the program." << endl;
			exit(0);
		}
	}
	//check whether each number is less than block size

	if(permutation[blockSize-1] <= 0 || permutation[blockSize] > 0)
	{
		cout << "Please enter numbers match block size, re-run the program." << endl;
		exit(0);
	}
	//check integer numbers match block size

	for(int i = 0; i < blockSize; i++)
	{
		for(int j = i + 1; j < blockSize; j++)
		{
			if(permutation[i] == permutation[j])
			{
				cout << "Every permutation number must be different, re-run the program." << endl;
				exit(0);
			}
		}
	}
	//check are there exist two or more equal numbers
}

void sort(int c[])
{
	int compare = 1;

	for(int i = 0; i < blockSize; i++)
	{
		for(int j = 0; j < blockSize; j++)
		{
			if(c[j] == compare)
			{
				depermutation[i] = j;
				compare++;

				break;
			}
		}
	}
	//order permutation for decrypt
}

void encrypt(int c[])
{
	int count = 0;

	for(int i = 0; i < length; i++)
	{
		if(plaintext[i] == ' ')
		{
			for(int j = i; j < length; j++)
			{
				plaintext[j] = plaintext[j+1];
			}
			length--;
		}
	}
	//remove spaces

	while(length % blockSize != 0)
	{
		plaintext[length] += 'x';
	    length++;
	    fillX++;
	}
	//fill 'x' and count how many 'x'

	while (count < length)
	{
		if ((count + 1) % blockSize == 0)
		{
			for(int i = 0; i < blockSize; i++)
			{
				encode += plaintext[c[i]-1];
				c[i] += blockSize;
			}

			count ++;
		}
		else
		{
			count++;
		}
	}
	//encrypt

	cout << "Encrypted message:" << endl;
	cout << encode << endl;
}

void decrypt(int c[])
{
	string decode;
	int count = 0;

	while (count < length)
	{
		if ((count + 1) % blockSize == 0)
		{
			for(int i = 0; i < blockSize; i++)
			{
				decode += encode[c[i]];
				c[i] += blockSize;
			}

			count++;
		}
		else
		{
			count++;
		}
	}
	//decrypt with extra 'x'

	if(decode[length - 1] == 'x')
	{
		while(deleteX < fillX)
		{
			deleteX++;
			decode[length - deleteX] = 0;
		}
	}

	//delete 'x'

	cout << "Decrypted message:" << endl;
	cout << decode << endl;
}

void possibleP()
{
	cout << "There are possible permutation of encoded message:" << endl;

	int p[blockSize] = {0,1,2,3};

	sort(p, p + blockSize);

	do
	{
		for(int i = 0; i < blockSize; i++)
		{
			possiblePermutation[i] = p[i];
		}

		decrypt(possiblePermutation);

	}while(next_permutation(p, p + blockSize));
	//decrypt every possible permutation of encoded message
}
